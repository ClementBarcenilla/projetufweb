<?php
session_start ();
require_once ('bddconnec.php');
?>


<!DOCTYPE html>

<html lang="en">
  <head>
    <title>Clément Barcenilla &mdash; Portfolio</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Portfolio Clément Barcenilla." name="description">


    
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

  </head>
  <body data-spy="scroll" data-target="#pb-navbar" data-offset="200">
  
    <nav class="navbar navbar-expand-lg site-navbar navbar-light bg-light" id="pb-navbar">

      <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <a class="navbar-brand" href="index.php">Clément Barcenilla</a>
        <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample09">
          <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="#section-home">Accueil</a></li>
            <li class="nav-item"><a class="nav-link" href="#section-about">A Propos</a></li>
            <li class="nav-item"><a class="nav-link" href="#section-portfolio">Projets</a></li>
            <li class="nav-item"><a class="nav-link" href="#section-resume">Résumé</a></li>
            <li class="nav-item"><a class="nav-link" href="#section-contact">Contact</a></li>
          </ul>
        </div>
      </div>
    </nav>
    

   
    
    <section class="site-hero" style="background-image: url(images/background.jpg);" id="section-home" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row intro-text align-items-center justify-content-center">
          <div class="col-md-10 text-center">
            <h1 class="site-heading site-animate">Salut! Je suis <br> <strong>Clément Barcenilla</strong></h1>
            <p class="lead site-subheading mb-4 site-animate">Bienvenur sur mon Portfolio!</p>
            <p><a href="#section-about" class="smoothscroll btn btn-primary px-4 py-3">À Propos</a></p>
          </div>
        </div>
      </div>
    </section> 


    <!-- section -->

    <section class="site-section bg-light" id="section-about">
      <div class="container">
        <div class="row mb-5 align-items-center">
          <div class="col-lg-7 pr-lg-5 mb-5 mb-lg-0">
            <img src="images/background.jpg" alt="Image placeholder" class="img-fluid">
          </div>
          <div class="col-lg-5 pl-lg-5">
            <div class="section-heading">
              <h2>À <strong>Propos</strong></h2>
            </div>
            <p class="lead">Je suis un élève un première année d'informatique à Ynov Aix en Provence</p>
            <p class="mb-5  ">A COMPLETER</p>

            <p>
              <a href="#section-contact" class="btn btn-primary px-4 py-2 btn-sm smoothscroll">Me Contacter</a>
              <a href="cv/CV.pdf" target="blank" class="btn btn-secondary px-4 py-2 btn-sm">Mon CV</a>
            </p>
          </div>
        </div>

        <div class="row pt-6">
          <div class="col-md-3 mb-3">
            <div class="section-heading">
              <h2>Mes <strong>Compétences</strong></h2>
            </div>
          </div>
          <div class="col-md-9">
            <div class="skill">
              <h3>HTML/CSS</h3>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width:
                <?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='HTML/CSS'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?>"
                  aria-valuemin="0" aria-valuemax="100">
                  <span><?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='HTML/CSS'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?></span>
                </div>
              </div>
            </div>

            <div class="skill">
              <h3>Langage C</h3>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width:
                <?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='Langage C'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?>"
                  aria-valuemin="0" aria-valuemax="100">
                  <span><?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='Langage C'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?></span>
                </div>
              </div>
            </div>

            <div class="skill">
              <h3>JavaScript</h3>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width:
                <?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='JavaScript'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?>"
                  aria-valuemin="0" aria-valuemax="100">
                  <span><?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='JavaScript'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?></span>
                </div>
              </div>
            </div>

            <div class="skill">
              <h3>PHP</h3>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width:
                <?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='PHP'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?>"
                  aria-valuemin="0" aria-valuemax="100">
                  <span><?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='PHP'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?></span>
                </div>
              </div>
            </div>

            <div class="skill">
              <h3>Python</h3>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width:
                <?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='Python'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?>"
                  aria-valuemin="0" aria-valuemax="100">
                  <span><?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='Python'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?></span>
                </div>
              </div>
            </div>

            <div class="skill">
              <h3>SQL</h3>
              <div class="progress">
                <div class="progress-bar" role="progressbar" style="width:
                <?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='SQL'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?>"
                  aria-valuemin="0" aria-valuemax="100">
                  <span><?php
                    $sql = $pdo->prepare("SELECT Value FROM skill WHERE Nom ='SQL'");
                    $sql->execute();
                    $data = $sql->fetch(PDO::FETCH_NUM);
                  foreach($data as $val){
                      echo "$val" . "%";
                  }?></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>*/

    <section class="site-section" id="section-portfolio">
      <div class="container">
        <div class="row">
          <div class="section-heading text-center col-md-12">
              <h2>Mes <strong>Projets</strong></h2>
            </div>
        </div>
        <div class="filters">
                    </div>
                    
                    <div class="filters-content">
                        <div class="row grid">
                            <div class="single-portfolio col-sm-4 all cours">
                              <div class="relative">
                                <div class="thumb">
                                  <div class="overlay overlay-bg"></div>
                                   <a href="plante/index.php"><img class="image img-fluid" src="images/ProjetDev.png" alt="ProjetDevPic"/></a>
                                </div>
                  <a href="" class="img-pop-up">  
                    <div class="middle">
                      <div class="text align-self-center d-flex"><img src="" alt=""></div>
                    </div>
                </a>                                  
                              </div>
                <div class="p-inner">
                    <h4></h4>
                  <div class="cat"></div>
                </div>                                         
                            </div>
                            <div class="single-portfolio col-sm-4 all cours">
                              <div class="relative">
                                <div class="thumb">
                                  <div class="overlay overlay-bg"></div>
                                   <img class="image img-fluid" src="images/workinprogress.png" alt="">
                                </div>
                  <a href="" class="img-pop-up">  
                    <div class="middle">
                      <div class="text align-self-center d-flex"><img src="" alt=""></div>
                    </div>
                </a>                                  
                              </div>
                <div class="p-inner">
                    <h4></h4>
                  <div class="cat"></div>
                </div>                                         
                            </div>                            
                            <div class="single-portfolio col-sm-4 all perso">
                              <div class="relative">
                                <div class="thumb">
                                  <div class="overlay overlay-bg"></div>
                                   <img class="image img-fluid" src="images/workinprogress.png" alt="">
                                </div>
                  <a href="" class="img-pop-up">  
                    <div class="middle">
                      <div class="text align-self-center d-flex"><img src="" alt=""></div>
                    </div>
                  </a> 
                                  
                              </div>
                                <div class="p-inner">
                                    <h4></h4>
                                    <div class="cat"></div>
                                </div>
                            </div>
                            <div class="single-portfolio col-sm-4 all packaging">
                              <div class="relative">
                                <div class="thumb">
                                  <div class="overlay overlay-bg"></div>
                                   <img class="image img-fluid" src="images/workinprogress.png" alt="">
                                </div>
                  <a href="" class="img-pop-up">  
                    <div class="middle">
                      <div class="text align-self-center d-flex"><img src="" alt=""></div>
                    </div>
                  </a>                                
                              </div> 
                                <div class="p-inner">
                                    <h4></h4>
                                    <div class="cat"></div>
                                </div>
                            </div>
                            <div class="single-portfolio col-sm-4 all typography">
                              <div class="relative">
                              <div class="thumb">
                                  <div class="overlay overlay-bg"></div>
                                   <img class="image img-fluid" src="images/workinprogress.png" alt="">
                                </div>
                  <a href="" class="img-pop-up">  
                    <div class="middle">
                      <div class="text align-self-center d-flex"><img src="" alt=""></div>
                    </div>
                  </a>                                
                              </div>
                                <div class="p-inner">
                                    <h4></h4>
                                    <div class="cat"></div>
                                </div>
                            </div>
                            <div class="single-portfolio col-sm-4 all photography">
                              <div class="relative">
                                <div class="thumb">
                                  <div class="overlay overlay-bg"></div>
                                   <img class="image img-fluid" src="images/workinprogress.png" alt="">
                                </div>
                  <a href="" class="img-pop-up">  
                    <div class="middle">
                      <div class="text align-self-center d-flex"><img src="" alt=""></div>
                    </div>
                  </a>                                
                              </div>
                                <div class="p-inner">
                                    <h4></h4>
                                    <div class="cat"></div>
                                </div>
                            </div>
                        </div>
                    </div>
      </div>
    </section>
    <!-- .section -->

    <section class="site-section bg-light " id="section-resume">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-5">
            <div class="section-heading text-center">
              <h2>Mon <strong>Parcours</strong></h2>
            </div>
          </div>
          <div class="col-md-6">
            <h2 class="mb-5">Éducation</h2>
            <div class="resume-item mb-4">
              <span class="date"><span class="icon-calendar"></span> Septembre 2010 - Juin 2014</span>
              <h3>Collège Viala Lacoste</h3>
              <p>J'y ai obtenu mon brevet série générale en 2015 mention <strong>assez bien</strong>.</p>
              <span class="school">Viala Lacoste - Salon de Pce</span>
            </div>

            <div class="resume-item mb-4">
              <span class="date"><span class="icon-calendar"></span> Septembre 2014 - Juin 2016</span>
              <h3>Lycée Viala Lacoste</h3>
              <p>J'y ai passé mes 2 Secondes générales</p>
              <span class="school">Viala Lacoste - Salon de Pce</span>
            </div>

            <div class="resume-item mb-4">
              <span class="date"><span class="icon-calendar"></span> Septembre 2016 - Juin 2018</span>
              <h3>Lycée Adam de Craponne</h3>
              <p>J'y ai obtenu mon brevet BAC ES en 2018 mention <strong>assez bien</strong></p>
              <span class="school">Adam de Crapone - Salon de Pce</span>
            </div>

            <div class="resume-item mb-4">
              <span class="date"><span class="icon-calendar"></span> 2018 - Present</span>
              <h3>INÉSUP Informatique</h3>
              <p>Je compte y faire 5 années d'étude afin d'obtenir un Mastère en <strong>sécurité informatique</strong>.</p>
              <span class="school">Ynov - Aix en Pce/span>
            </div>

          </div>
          <div class="col-md-6">
            

            <h2 class="mb-5">Expérience</h2>

            <div class="resume-item mb-4">
              <span class="date"><span class="icon-calendar"></span> Juin 2017 - Juillet 2017</span>
              <h3>Assitant Service Technique</h3>
              <p>Service grand ménage dans les écoles de Saint Cannat (Nettoyage et rangement des salles de classe)</p>
              <span class="school">Maire de Saint Cannat</span>
            </div>

            <div class="resume-item mb-4">
              <span class="date"><span class="icon-calendar"></span> Juin 2019 - Septembre 2019</span>
              <h3>Employé Mc Donald</h3>
              <p>Je vais être employé chez Mc Donalds afin de compléter ma première année à Ynov</strong></p>
              <span class="school">Mc Donald Saint Cannat</span>
            </div>

          </div>
        </div>
      </div>
    </section>
     <!-- .section -->


    <section class="site-section bg-light" id="section-contact">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-5">
            <div class="section-heading text-center">
              <h2>Vous <strong>souhaitez</strong> me contacter ?</h2>
            </div>
          </div>
          
          <div class="col-md-7 mb-5 mb-md-0">
            <form action="php/mail.php" method="POST" class="site-form">
              <h3 class="mb-5">Me contacter</h3>
              <div class="form-group">
                <input type="text" class="form-control px-3 py-4" placeholder="Nom" id="name" maxlength="50" name="nom">

              </div>
              <div class="form-group">
                <input type="email" class="form-control px-3 py-4" placeholder="Email" id="email" maxlength="100" name="email">

              </div>
              <div class="form-group mb-5">
                <textarea class="form-control px-3 py-4"cols="30" rows="10" placeholder="Votre message" id="message" maxlength="240" name="message"></textarea>

              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-primary  px-4 py-3" value="Envoyer">
              </div>
            </form>
          </div>
          <div class="col-md-5 pl-md-5">
            <h3 class="mb-5">Contact Details</h3>
            <ul class="site-contact-details">
              <li>
                <span class="text-uppercase">Email</span>
                clement.barcenilla@ynov.com
              </li>
              <li>
                <span class="text-uppercase">Téléphone</span>
                +33 6 14 65 ** **
              </li>

              <li>
                <span class="text-uppercase">Adresse</span>
                Aix en Provence, FR <br>
                Batîment Centre Aix  <br>
                Ynov Campus
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>


    <footer class="site-footer">
      <div class="container">
        <div class="row mb-3">
          <div class="col-md-12 text-center">
            <p>
              <a href="https://twitter.com/OutIxw" target="blank" class="social-item"><span class="icon-twitter"></span></a>
              <a href="https://www.linkedin.com/in/cl%C3%A9ment-barcenilla-604047171/" target="blank" class="social-item"><span class="icon-linkedin2"></span></a>
              <a href="https://gitlab.com/ClementBarcenilla?nav_source=navbar" target="blank" class="social-item"><span class="icon-github"></span></a>
            </p>
          </div>
        </div>
        <div class="row">
            <p class="col-12 text-center">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | Clément Barcenilla | Template by <a href="https://colorlib.com" target="_blank" class="text-primary">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
        </div>
      </div>
    </footer>


    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/vendor/jquery.easing.1.3.js"></script>
    <script src="js/vendor/jquery.stellar.min.js"></script>
    <script src="js/vendor/jquery.waypoints.min.js"></script>

    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="js/custom.js"></script>

    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script>

  </body>
</html>