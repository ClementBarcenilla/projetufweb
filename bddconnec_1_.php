<?php
$param = parse_ini_file("db.ini");
try{
    $pdo = new PDO("mysql:host=".$param['url'].";dbname=".$param["db"], $param["user"], $param["password"]);
} catch (PDOException $e) {
    echo $e->getCode() . "<br/>";
    echo $e->getMessage() . "<br/>";
}